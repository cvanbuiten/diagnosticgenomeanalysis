Project Diagnostics Genome Analysis
Author: Carlo van Buiten
Student ID: 393539

The goal of this project is to identify mutations in the DNA that are disease causing for cardiomyopathy.
The scripts in this project work together to extract valuable data from .fastq files and load it into a MySQL database.


#Prerequisites
Python 3.7 -            is needed to run all the scripts
MySQL      -            is needed to run all the scripts and load the data into a database
Galaxy     -            Trimmomatic, Map with BWA-MEM, MarkDuplicates, MPileUp, VarScan, ANNOVAR tools needed to obtain a .tsv file if you do not already have the data to input into deliverable9.py
Database   -            In order to insert the final data into a database, you must have a database on the mariadb.bin server.
Input data -            in the best case scenario you already have a .tsv file to put in deliverable9.py, you can, however, also start with a .fastq file


#Imports
sys                 -            is needed to run all the scripts
csv                 -            is needed to run deliverable3.py and deliverable4.py
argparse            -            is needed to run deliverable5.py and deliverable9.py
json                -            is needed to print the output in annovar.py
operator            -            is needed to run annovar.py and deliverable9.py
re                  -            is needed to run deliverable7.py and deliverable9.py
mysql.connector     -            is needed to run deliverable9.py and load the data into a database


#Contents
deliverable1.py     -            Contains a function for deliverable4.py that parses data from a BED file.
deliverable2.py     -            Contains a function for deliverable4.py that creates a coverage dictionary from BED data.
deliverable3.py     -            Contains functions for deliverable4.py that read a BED file, calculate coverage statistics.
deliverable4.py     -            Takes BED and PILEUP files as input and calculates and writes coverage data to a new .csv file.
deliverable5.py     -            Takes a VCF file as input, filters out variants below a given threshold and writes the rest to a new VCF file.
deliverable6.py     -            Extracts valuable data from a given .tsv file and print it.
annovar.py          -            Extracts valuable data from a given .tsv file and stores it, to be used in deliverable9.py.
deliverable7.py     -            Contains a function for deliverable9.py that extracts the true gene name from the RefSeq_Gene column in the .tsv file.
deliverable8.dia    -            Design template from deliverable8.sql.
deliverable8.sql    -            SQL script that makes the empty tables that are to be filled by deliverable9.py.
deliverable9.py     -            Final program that extracts valuable data from a .tsv file, converts it to the proper data types and stores it in a MySQL database.


#Instructions
deliverable1.py is for testing purposes only.
deliverable2.py is for testing purposes only.
deliverable3.py is for testing purposes only.
deliverable6.py is for testing purposes only.
annovar.py is for testing purposes only.
deliverable7.py is for testing purposes only.

If you already have a .tsv file, skip to deliverable8.sql.
If you only have a .fastq file, follow the full instructions instead:

In Galaxy, use the Trimmomatic tool on your initial .fastq file, this will provide you with trimmed reads.
Then use the Map with BWA-MEM tool on your trimmed .fastq file to map your reads.
Then use the MarkDuplicates tool on your mapped reads from the mapping tool.
Then use the MPileUp tool on your mapped reads with marked duplicates to obtain a .pileup file.

At this point, deliverable4.py can be run to provide you with a .csv file containing coverage statistics.
deliverable4.py is run on the commandline as follows: python3 deliverable4.py [bed_file.bed] [pileup_file.pileup]

Run the VarScan tool on the .pileup file to obtain a .vcf file.
Now run deliverable5.py on the commandline as follows: python3 deliverable5.py [vcf_file.vcf] [frequency] [out_file.vcf]
This provides you with a filtered .vcf file containing only the variants with a higher frequency than your given frequency.
You must upload this .vcf file to Galaxy and run the ANNOVAR tool on it. You now have a .tsv file.

Execute deliverable8.sql to create empty tables in your database.

Now run deliverable9.py from the commandline as follows: python3 deliverable9.py [database_name] [username] [password] [your_input.tsv]

The program has now filled the tables from deliverable8.sql with the valuable columns from your .tsv file.

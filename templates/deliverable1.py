#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project
For testing purposes only, to be used in deliverable4.py
"""


__author__ = "Carlo van Buiten"
__version__ = "2018.d1.v1"


import sys


def parse_bed_data(bed_data):
    """ Function that parses BED data and stores its contents
        in a dictionary
    """
    bed_dict = {}
    previous = 0    # Value to keep track of what chromosome the program is looking at
    for line in bed_data:
        entry = line.split()
        chrom = entry[0]
        start = int(entry[1])
        stop = int(entry[2])
        gen = entry[3]
        if chrom != previous:   # Checks if this is a new chromosome
            bed_dict[chrom] = [(start, stop, gen)]  # Makes a new chromosome entry if this is a new chromosome
        else:
            bed_dict[chrom].append((start, stop, gen))

        previous = chrom    # Keeps track of the chromosome name

    print(bed_dict)

    return bed_dict


def main():
    """ Main function that tests for correct parsing of BED data """
    # Example input
    bed_data = [
        "1	237729847	237730095	RYR2",
        "1	237732425	237732639	RYR2",
        "1	237753073	237753321	RYR2",
        "18	28651551	28651827	DSC2",
        "18	28654629	28654893	DSC2",
        "18	28659793	28659975	DSC2",
        "X	153648351	153648623	TAZ",
        "X	153648977	153649094	TAZ",
        "X	153649222	153649363	TAZ"]

    # Expected output for the example input
    expected_bed_dict = {
        '1':  [(237729847, 237730095, 'RYR2'),
               (237732425, 237732639, 'RYR2'),
               (237753073, 237753321, 'RYR2')],
        '18': [(28651551, 28651827, 'DSC2'),
               (28654629, 28654893, 'DSC2'),
               (28659793, 28659975, 'DSC2')],
        'X':  [(153648351, 153648623, 'TAZ'),
               (153648977, 153649094, 'TAZ'),
               (153649222, 153649363, 'TAZ')]}

    # Call the parse-function
    bed_dict = parse_bed_data(bed_data)
    _assert_output_vs_expected(bed_dict, expected_bed_dict)


def _assert_output_vs_expected(output, expected):
    """ Compares given output with expected output.
    Do not modify. """
    import unittest
    if isinstance(output, dict):
        testcase = unittest.TestCase('__init__')
        try:
            testcase.assertDictEqual(expected, output,
                                     msg="\n\nUnfortunately, the output is *not* correct..")
        except AssertionError as error:
            print(error)
            return 0
        print("\nWell done! Output is correct!")
        return 1
    print("\n\nUnfortunately, the output is *not* a dictionary!")
    return 0


if __name__ == '__main__':
    sys.exit(main())

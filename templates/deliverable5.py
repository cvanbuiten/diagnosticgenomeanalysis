#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project
    usage:
        python3 deliverable5.py vcf_file.vcf frequency out_file.vcf

    arguments:
        vcf_file.vcf: the input VCF file, output from the varscan tool
                      frequency: a number (integer) to use as filtering value
        out_file.vcf: name of the output VCF file 

    output:
        a VCF file containing the complete header (comment lines) and
        only the remaining variant positions after filtering.
"""


__author__ = "Carlo van Buiten"
__version__ = "2019.d5.v1"


import sys
import argparse


def parse_vcf_data(vcf_in, freq, vcf_out):
    """ This function reads the input VCF file line by line, skipping the first
    n-header lines. The remaining lines are parsed to filter out variant allele
    frequencies > frequency.
    """

    # Open the INPUT VCF file, read the contents line-by-line
    # Write the first ... comment-lines (header) directly to the output file
    input_file = open(vcf_in)
    output_file = open(vcf_out, "a")
    for line in input_file:
        if line.startswith("#"):
            output_file.write(line)
        elif line.startswith("chr"):
            temp = line.split()
            line_freq = temp[9].split(":")[6]
            if float(line_freq[:-1]) > freq:    # Writes output to file if frequency exceeds threshold
                output_file.write(line)

    input_file.close()
    output_file.close()


def main():
    """ Main function """

    # Try to read input arguments from the commandline.
    parser = argparse.ArgumentParser(description="Filter VCF data.")
    parser.add_argument("vcf_in", help="Your VCF input file.")
    parser.add_argument("freq", type=int, help="Your desired minimum frequency.")
    parser.add_argument("vcf_out", help="The name for the output VCF.")

    args = parser.parse_args()

    # Process the VCF-file
    parse_vcf_data(args.vcf_in, args.freq, args.vcf_out)

    return 0


if __name__ == "__main__":
    sys.exit(main())

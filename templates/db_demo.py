#!/usr/bin/env python3

"""
Example program demonstrating inserting data originating from a
dictionary into tables.
"""

import sys
from dbconnector import cnx, create_tables

def insert_into_table(table, data):
    """ Filters the correct columns from the data given the table name
	See https://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-transaction.html
	for a description on including data from a dictionary in the string-formatting
    """
    cursor = cnx.cursor()

    # Get the description for the requested table
    cursor.execute("desc %s" % table)
    tdescription = cursor.fetchall()

    # Get the actual columns from the table
    table_columns = [column[0] for column in tdescription]
    # Match with the columns in the input-data
    data_columns = [column for column in table_columns if column in data]

    query = "INSERT INTO {} ({}) VALUES ({})".format(table,
                                                     # List the column names
                                                     ', '.join(data_columns),
                                                     # Format the VALUES as '(%(name)s, %(chr_id)s)'
                                                     ', '.join(['%({})s'.format(k) for k in data_columns]))
    cursor.execute(query, data)

    # Get the ID field value (primary key) of the inserted row
    return cursor.lastrowid

def fill_tables():
    # Fake data for three 'lines' of Annovar data
    annovar = [
            {'chr_name': 'chr1', 'gene_name': 'RYR2', 'some_annotation': 0.023},
            {'chr_name': 'chr2', 'gene_name': 'MYHD6', 'some_annotation': 0.339},
            {'chr_name': 'chr3', 'gene_name': 'TTN', 'some_annotation': 0.088}
           ]


    # Process data 'line-by-line'
    for i, variant in annovar:
        # Store the ID of the inserted chromosome
        annovar[i]['chr_id'] = insert_into_table('Chromosomes', variant)
        # Add the gene, using the 'chr_id' field
        insert_into_table('Genes', variant)

def main(args):
    """ Main function responsible for creating new, empty tables and filling them """
    create_tables()
    fill_tables()

    # Important, apply changes to the DB
    cnx.commit()
    cnx.close()

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))

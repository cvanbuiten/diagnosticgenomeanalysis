#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project
For testing purposes only, to be used in deliverable4.py
"""


__author__ = "Carlo van Buiten"
__version__ = "2018.d2.v1"


import sys


def parse_pileup_data(pileup_data, bed_dict):
    """ Function that parses pileup data and collects the per-base coverage
    of all exons contained in the BED data.

    Iterate over all pileup lines and for each line:
        - check if the position falls within an exon (from `bed_dict`)
            - if so; add the coverage to the `coverage_dict` for the correct gene
    """

    coverage_dict = {}

    for line in pileup_data:
        data = line.split()
        chrom = data[0][3:]     # Removes the 'chr' text from the chromosome, leaving just the number
        if chrom in bed_dict:   # Checks if the chromosome is contained in the bed dict
            coord = data[1]
            for exon in bed_dict[chrom]:
                if exon[0] <= int(coord) <= exon[1]:    # Checks if the position is located on an exon
                    if exon[2] in coverage_dict:    # Checks if an entry exists for the current gene
                        coverage_dict[exon[2]].append(int(data[3]))     # Adds coverage value to the gene
                    else:
                        coverage_dict[exon[2]] = [int(data[3])]     # Makes a new gene entry

    return coverage_dict


def main():
    """ Main function with example input data (pileup and parsed bed)"""

    # Example pileup data with chromosome, position, base, coverage, reads, quality
    pileup_data = [
        'chr1	839427	A	24	,,,,,,,,,,,,,,,,,,,,,,,,	BFGGGGGGGGHHGHH3AFHHIGFG',
        'chr1	237732518	T	24	,,,,,,,,,,,,,,,,,,,,,,,,	>FFFGHHHCDHHHHGF>CHHHHHA',
        'chr1	237732519	T	24	,,,,,,,,,,,,,,,,,,,,,,,,	>FFFGHHHCDHHHHGF>CHHHHHA',
        'chr1	237732520	T	26	,,,,,,,,,,,,,,,,,,,,,,,,,,	>FFFGHHHCDHHHH5GGF>CHHHHHA',
        'chr1	237732521	T	27	,,,,,,,,,,,,,,,,,,,,,,,,,,,	>FFFGHHHCDHHHH6GFGF>CHHHHHA',
        'chr3	1290	T	24	,$,,,,,,,,,,,,,,,,$,,,,,,,	;FFFGGCGGFHHHFHF;FFHHHFD',
        'chr4	123383120	A	22	.....................^].	>HHHHGHHBHHFGGG5GD1ACA',
        'chr12	78383124	G	22	.$....................^].	;HBH1GFAHHHHFGDGGGFFC>',
        'chr12	78383132	C	22	.....................^].	HHHHHBHHGHHHHHGGGGFC1B',
        'chr18	28651722	A	23	......................^].	GHFHHFHGAHHHHHFGGGFFABB',
        'chr18	28659880	C	23	.......................	HHHHHHHHCHH4HHGFGGFF?BA',
        'chr18	28659881	C	23	.......................	HHHHHHHHCHH4HHGFGGFF?BA',
        'chr18	28659882	C	19	...................	HHHHCHH4HHGFGGFF?BA',
        'chr18	28659883	C	19	...................	HHHHCHH4HHGFGGFF?BA',
        'chrX	9402753	A	23	.......................	HHHGGHG/AHHHHHHG4GBBABB'
    ]

    # Example BED data with chromosome (key), list of tuples with (start, stop and gene name)
    bed_dict = {
        '1':  [(237729847, 237730095, 'RYR2'),
               (237732425, 237732639, 'RYR2'),
               (237753073, 237753321, 'RYR2')],
        '18': [(28651551, 28651827, 'DSC2'),
               (28654629, 28654893, 'DSC2'),
               (28659793, 28659975, 'DSC2')]}

    # Expected output for example input
    expected_coverage_dict = {
        "RYR2": [24, 24, 26, 27],
        "DSC2": [23, 23, 23, 19, 19]
    }

    coverage_dict = parse_pileup_data(pileup_data, bed_dict)    # Calls parse function to create the dict
    _assert_output_vs_expected(coverage_dict, expected_coverage_dict)   # Compares output to expected output

    return 0


def _assert_output_vs_expected(output, expected):
    """ Compares given output with expected output.
    Do not modify. """
    import unittest
    if isinstance(output, dict):
        testcase = unittest.TestCase('__init__')
        try:
            testcase.assertDictEqual(expected, output,
                                     msg="\n\nUnfortunately, the output is *not* correct..")
        except AssertionError as error:
            print(error)
            return 0
        print("\nWell done! Output is correct!")
        return 1
    print("\n\nUnfortunately, the output is *not* a dictionary!")
    return 0


if __name__ == '__main__':
    sys.exit(main())

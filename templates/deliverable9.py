#!/usr/bin/env python3

"""
Final program that extracts valuable columns from an ANNOVAR.tsv file and writes the data to a MySQL database
"""

import sys
import argparse
import mysql.connector
import re
from operator import itemgetter


def get_gene_name(gene_name):
    """This function accepts a string that contains a gene name among other non-important data and returns
    the gene name"""
    elements = []
    pattern = re.compile(r'[A-Z0-9]{3,}')

    for match in pattern.findall(gene_name):
        if not match.startswith(("LOC", "LIN", "NONE")) and not re.match(r'\d', match):
            elements.append(match)

    if not elements:    # If gene name is missing, inserts a '-'
        elements.append("-")

    gene = "/".join(elements)
    return gene


def parse_annovar(filename):
    """Function parses an ANNOVAR.tsv file and returns a list object with dictionaries per row"""
    header = []
    annovar_data = []

    with open(filename) as data:
        for i, variant in enumerate(data):

            # Get header if we're processing the first line
            if i == 0:
                header = process_annovar_line(variant, header=False)
                continue  # proceed to next line

            # Process the data and add to a list
            annovar_data.append(process_annovar_line(variant, header))

    return annovar_data


def process_annovar_line(variant, header):
    """ Given a list of column indices, parse the header and all data lines """
    # Define the columns of interest
    columns = [0, 6, 15, 16, 27, 33, 34, 35, 36, 53]
    data = variant.split("\t")

    # Extract values for selected columns
    annotation_values = itemgetter(*columns)(data)

    # Return list of values if header is requested
    if not header:
        return annotation_values

    # Return all data as a dictionary with column: value
    return dict(
                zip(
                    [field.strip() for field in header],
                    [field.strip() for field in annotation_values]
                )
            )


def fill_chromosomes(variant):
    """Inserts chromosome data into the chromosome table in the database"""
    cursor = cnx.cursor()
    add_chromosome = ("INSERT INTO chromosomes "
                      "(chromosome, chromosome_id) "
                      "VALUES (%(chromosome)s, %(chromosome_id)s)")

    cursor.execute(add_chromosome, variant)

    cnx.commit()
    cursor.close()


def fill_genes(variant):
    """Inserts gene data into the gene table in the database"""
    cursor = cnx.cursor()
    add_gene = ("INSERT INTO genes "
                "(RefSeq_Gene, chromosome_id, gene_id) "
                "VALUES (%(RefSeq_Gene)s, %(chromosome_id)s, %(gene_id)s)")

    cursor.execute(add_gene, variant)

    cnx.commit()
    cursor.close()


def fill_variants(variant):
    """Inserts variant data into the variant table in the database"""
    cursor = cnx.cursor()
    add_variant = ("INSERT INTO variants "
                   "(dbsnp138, POS, RefSeq_Func, 1000g2015aug_EUR, LJB2_SIFT, LJB2_PolyPhen2_HVAR, LJB2_PolyPhen2_HDIV,"
                   "CLINVAR, gene_id) "
                   "VALUES (%(dbsnp138)s, %(POS)s, %(RefSeq_Func)s, %(1000g2015aug_EUR)s, %(LJB2_SIFT)s, "
                   "%(LJB2_PolyPhen2_HVAR)s, %(LJB2_PolyPhen2_HDIV)s, %(CLINVAR)s, %(gene_id)s)")

    cursor.execute(add_variant, variant)

    cnx.commit()
    cursor.close()


def main():
    """ Main function responsible for creating new, empty tables and filling them """
    chrom_list = []     # List that keeps track of chromosomes that have already been seen
    gene_list = []      # List that keeps track of genes that have already been seen
    temp_chrom = 0      # Counter for the chromosome ID column
    temp_gene = 0       # Counter for the gene ID column

    vlist = parse_annovar(args.tsv)

    for variant in vlist:   # Loops through the list of variant-dictionaries
        variant["RefSeq_Gene"] = get_gene_name(variant["RefSeq_Gene"])  # Calls the function to get the gene names
        # Checks if chromosome has been seen before, if this is not the case, increments chromosome ID and inserts data
        if variant["chromosome"] not in chrom_list:
            temp_chrom += 1
            variant["chromosome_id"] = temp_chrom
            fill_chromosomes(variant)
            chrom_list.append(variant["chromosome"])
        else:
            variant["chromosome_id"] = temp_chrom
        # Checks if gene has been seen before, if this is not the case, increments gene ID and inserts data
        if variant["RefSeq_Gene"] not in gene_list:
            temp_gene += 1
            variant["gene_id"] = temp_gene
            fill_genes(variant)
            gene_list.append(variant["RefSeq_Gene"])
        else:
            variant["gene_id"] = temp_gene

        variant["POS"] = int(variant["POS"])    # Turns the data into the proper data type

        if variant["1000g2015aug_EUR"]:     # Turns the data into the proper data type
            variant["1000g2015aug_EUR"] = float(variant["1000g2015aug_EUR"])
        else:
            variant["1000g2015aug_EUR"] = None

        if variant["LJB2_SIFT"]:        # Turns the data into the proper data type
            variant["LJB2_SIFT"] = float(variant["LJB2_SIFT"])
        else:
            variant["LJB2_SIFT"] = None

        fill_variants(variant)  # Inserts variant data

    # Applies changes to the database
    cnx.commit()
    cnx.close()

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Loads data from your .tsv file into a MySQL database.")
    parser.add_argument("db", help="Your database name.")
    parser.add_argument("us", help="Your username.")
    parser.add_argument("pw", help="Your password.")
    parser.add_argument("tsv", help="Your input ANNOVAR.tsv file.")
    args = parser.parse_args()

    cnx = mysql.connector.connect(user=args.us, password=args.pw,
                                  host='mariadb.bin',
                                  database=args.db)

    sys.exit(main())

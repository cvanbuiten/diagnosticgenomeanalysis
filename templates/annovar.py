#!/usr/bin/env python3

"""
Parses data from ANNOVAR.tsv files into a list of dictionaries per row

For testing purposes only, to be used in deliverable9.py
"""

import sys
import json
from operator import itemgetter


def parse_annovar(filename):
    header = []
    annovar_data = []

    with open(filename) as data:
        for i, variant in enumerate(data):

            # Get header if we're processing the first line
            if i == 0:
                header = process_annovar_line(variant, header=False)
                continue     # proceed to next line

            # Process the data and add to a list
            annovar_data.append(process_annovar_line(variant, header))

    return annovar_data


def process_annovar_line(variant, header):
    """ Given a list of column indices, parse the header and all data lines """
    # Define the columns of interest
    columns = [0, 6, 15, 16, 27, 33, 34, 35, 36, 53]
    data = variant.split("\t")

    # Extract values for selected columns
    annotation_values = itemgetter(*columns)(data)

    # Return list of values if header is requested
    if not header:
        return annotation_values

    # Return all data as a dictionary with column: value
    return dict(
                zip(
                    [field.strip() for field in header],
                    [field.strip() for field in annotation_values]
                )
            )


def main():
    """ Main function for processing Annovar annotation data """
  
    # Pretty-print the dictionary as JSON object
    print(json.dumps(parse_annovar("data/annovar.tsv"), indent=4))
    return 0


if __name__ == "__main__":
    sys.exit(main())

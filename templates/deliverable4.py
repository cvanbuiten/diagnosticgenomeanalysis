#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Combines deliverable1 2 and 3 to perform the following steps:
* Load the BED file containing the information (names, chromosome and
  coordinates of the exons) of all cardiopanel genes
* Load the pileup file containing the mapping data
* For each exon found in the BED file:
    * read the start- and end-coordinate
    * find all entries in the pileup file for this chromosome and within
      these coordinates
    * for each pileup-entry:
        * store the coverage (data from column 4)
* Given the found coverage for each position in all exons:
    * Calculate the average coverage per gene
    * Count the number of positions with a coverage < 30
* Write a report on all findings (output to Excel-like file)

* Note: Test the program on the example data first.
* Note: by default the 'data/example.bed' and 'data/example.pileup' files are
        used as input, but you can supply your own files on the
        commandline.

    usage:
        python3 deliverable4.py [bed-file.bed] [pileup-file.pileup]
"""

__author__ = "Carlo van Buiten"
__version__ = "2018.d4.v1"

import sys
import csv


def read_data(filename):
    """ This function reads in data and returns a list containing one
        line per element. """
    # Opens the file given the filename stored in 'filename'
    filelist = []
    file = open(filename)
    for line in file:
        filelist.append(line)
    # Returns a list where each line is a list element
    return filelist


def parse_bed_data(bed_data):
    """ Function that parses BED data and stores its contents
        in a dictionary
    """
    # Create empty dictionary to hold the data
    bed_dict = {}
    previous = 0    # Value to keep track of what chromosome the program is looking at
    for line in bed_data:
        entry = line.split()
        chrom = entry[0]
        start = int(entry[1])
        stop = int(entry[2])
        gen = entry[3]
        if chrom != previous:   # Checks if this is a new chromosome
            bed_dict[chrom] = [(start, stop, gen)]  # Makes a new chromosome entry if this is a new chromosome
        else:
            bed_dict[chrom].append((start, stop, gen))

        previous = chrom    # Keeps track of the chromosome name

    print(bed_dict)

    return bed_dict


def parse_pileup_data(pileup_data, bed_dict):
    """ Function that parses pileup data and collects the per-base coverage
    of all exons contained in the BED data.

    Iterate over all pileup lines and for each line:
        - check if the position falls within an exon (from `bed_dict`)
            - if so; add the coverage to the `coverage_dict` for the correct gene
    """

    # Create empty dictionary to hold the data
    coverage_dict = {}

    for line in pileup_data:
        data = line.split()
        chrom = data[0][3:]     # Removes the 'chr' text from the chromosome, leaving just the number
        if chrom in bed_dict:
            coord = data[1]
            for exon in bed_dict[chrom]:
                if exon[0] <= int(coord) < exon[1]:
                    if exon[2] in coverage_dict:
                        coverage_dict[exon[2]].append(int(data[3]))
                    else:
                        coverage_dict[exon[2]] = [int(data[3])]

    return coverage_dict


def calculate_mapping_coverage(coverage_dict):
    """ Function to calculate all coverage statistics on a per-gene basis
        and store this in a list.
        Note: this function is taken from deliverable 5 and slightly modified
    """

    # Create an empty list that will hold all data to save
    statistics = []

    # Iterate over all the genes in the coverage_dict getting the gene name
    # and list with coverage data for that gene
    for gene in coverage_dict:
        entry = (gene, len(coverage_dict[gene]), round(sum(coverage_dict[gene])/len(coverage_dict[gene]), 1),
                 sum(map(lambda x: x < 30, coverage_dict[gene])))
        statistics.append(entry)

    # The lambda function puts the following elements in a single tuple and append to the
    # statistics list.
    #      * Gene name,
    #      * Total positions (gene length covered)
    #      * Average Coverage (use round with one position)
    #      * Number of low-coverage positions (coverage value < 30)

    # Return the list of tuples holding the data
    return statistics


def save_coverage_statistics(coverage_file, coverage_statistics):
    """ Writes coverage data to a tabular file using Python's
        csv library: https://docs.python.org/3/library/csv.html#csv.writer
    """
    with open(coverage_file, 'w') as file:
        writer = csv.writer(file)
        writer.writerows(coverage_statistics)
    file.close()
    # Write the coverage_statistics to a CSV file
    pass


def main(args):
    """ Main function connecting all functions
        Note: the 'is None' checks that are done are only
        necessary for this program to run without error if
        not all functions are completed.
    """

    # Tries to read input en output filenames from the commandline. Uses defaults if
    # they are missing and warn if the extensions are 'wrong'.
    if len(args) > 1:
        bed_file = args[1]
        if not bed_file.lower().endswith('.bed'):
            print('Warning: given BED file does not have a ".bed" extension.')
        pileup_file = args[2]
        if not pileup_file.lower().endswith('.pileup'):
            print('Warning: given pileup file does not have a ".pileup" extension.')
        output_file = args[3]
    else:
        bed_file = 'data/example.bed'
        pileup_file = 'data/example.pileup'
        output_file = 'data/d4_output.csv'

    # Read BED data
    print('Reading BED data from', bed_file)
    bed_data = read_data(bed_file)
    if bed_data is None:
        print('No BED-data read...')
    else:
        print('\t> A total of', len(bed_data), 'lines have been read.\n')

    # Read Pileup data
    print('Reading pileup data from', pileup_file)
    pileup_data = read_data(pileup_file)
    if pileup_data is None:
        print('No Pileup-data read...')
    else:
        print('\t> A total of', len(pileup_data), 'lines have been read.\n')

    # Parsing BED data
    print('Parsing BED data...')
    bed_dict = parse_bed_data(bed_data)
    if bed_dict is None:
        print('BED-data not parsed!')
    else:
        print('\t> A total of', len(bed_dict.keys()), 'chromosomes have been stored.\n')

    # Parsing and filtering pileup data
    print('Parsing and filtering pileup-data...')
    coverage_dict = parse_pileup_data(pileup_data, bed_dict)
    if coverage_dict is None:
        print('Pileup data not parsed!')
    else:
        print('\t> Coverage of', len(coverage_dict.keys()), 'genes have been stored.\n')

    # Store calculated data
    print('Calculating coverage statistics...')
    coverage_statistics = calculate_mapping_coverage(coverage_dict)
    if coverage_statistics is None:
        print('No coverage statistics calculated!')
    else:
        print('\t> Statistics for', len(coverage_statistics), 'genes have been calculated.\n')

    # Write output data
    print('Writing the coverage statistics to', output_file)
    if coverage_statistics is None:
        print('Nothing to write, quitting...')
    else:
        save_coverage_statistics(output_file, coverage_statistics)
        from pathlib import Path
        csv_file_check = Path(output_file)
        if csv_file_check.is_file():
            print('\t> CSV file created, program finished.')
        else:
            print('\tCSV file', output_file, 'does not exist!')

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))

#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Extracts valuable data from an ANNOVAR.tsv file and prints it
*Not important for the project*
*For the script that extracts data for use in deliverable9.py, see annovar.py*
"""

__author__ = "Carlo van Buiten"
__version__ = "2018.d6.v1"


import sys


def read_annovar(inputfile):
    columns = [15, 16, 27, 33, 34, 35, 36, 53]  # Columns to be extracted
    inputfile = open(inputfile)
    for line in inputfile:
        content = line.split("\t")
        print("\t".join([content[i] for i in columns]))     # Prints columns

    inputfile.close()
    return 0


def main(args):
    inputfile = args[1]
    read_annovar(inputfile)
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))

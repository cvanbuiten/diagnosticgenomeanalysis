#!/usr/bin/env python3
import mysql.connector

# Connection object
cnx = mysql.connector.connect(user='', password='',
                              host='mysql.bin',
                              database='')

def create_tables():
    """ Creates new, empty tables """
    chromosome_table = """
    CREATE TABLE Chromosomes (
        id INTEGER NOT NULL AUTO_INCREMENT,
        name VARCHAR(10) NOT NULL,
        PRIMARY KEY (id)
    );"""

    cnx.cursor().execute(chromosome_table)
    cnx.commit()
